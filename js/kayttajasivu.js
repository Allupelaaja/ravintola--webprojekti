var tunnus = "";

//Tarkistetaan onko käyttäjä kirjautunut sisään
if ((!sessionStorage.getItem('tunnus'))) {
    alert("Et pääse käyttäjäsivulle kirjautumatta sisään")
    window.location.href="frontpage.html";
}

tunnus = sessionStorage.getItem("tunnus");
document.getElementById("index").innerHTML = "Kirjaudu ulos ("+tunnus+")";


document.getElementById("button").addEventListener("click",function () {
    var salasana = document.getElementById("salasana").value;
    var sala1 = document.getElementById("sala1").value;
    var sala2 = document.getElementById("sala2").value;
    changePassword(salasana, sala1,sala2);
});

document.getElementById("title").innerHTML = "Käyttäjän "+tunnus+" asiakastiedot.";
document.getElementById("otsikko").innerHTML = "Käyttäjän <b>"+tunnus+"</b> asiakastiedot.";
document.getElementById("taulukonnimi").innerHTML = "Käyttäjän <b>"+tunnus+"</b> tilaukset.";


//Tarkistaa täsmääkö antamat kaksi uutta salasanaa
function checkPasswords() {
    var sala1 = document.getElementById("sala1");
    var sala2 = document.getElementById("sala2");

    document.getElementById("samepass").innerHTML = "Uudet salasanat ei täsmää";
    document.getElementById("samepass").style.color = "red";

    if(sala1.value || sala2.value) {
        document.getElementById("samepass").style.display = "block";
        if (sala1.value === sala2.value) {
            document.getElementById("samepass").innerHTML = "Uudet salasanat täsmää";
            document.getElementById("samepass").style.color = "green";
        }
    } else {
        document.getElementById("samepass").style.display = "none";
    }
}

//Salasanan vaihtamisen funktio
function changePassword(salasana,sala1,sala2) {
    if (salasana.length === 0 || sala1.length === 0 || sala2.length === 0 ) {
        alert("Salasanat pitää olla asetettu");
        return;
    }
    if (!(sala1 === sala2)){
        alert("Uudet salasanat pitää olla samat");
        return;
    }
    if (tunnus ==="admin"){
        alert("Et voi vaihtaa adminin salasanaa");
        return;
    }
    if (tunnus ==="tuntematon"){
        alert("Et voi vaihtaa tuntemattoman salasanaa");
        return;
    }

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            if(xhttp.responseText == 1){ //Php palauttaa muuttuneiden rivien lukumäärän
                location.reload();
                alert("Salasanan vaihto onnistui")

            } else {
                alert("Salasanan vaihto epäonnistui tarkista tiedot")
            }
        }
    };
    xhttp.open("POST", "php/changepassword.php?tunnus="+tunnus+"&uusisalasana="+sala1+"&vanhasalasana="+salasana, true);
    xhttp.send();
}

//Päivittää käyttäjän tilaustaulukon
function updateOrderTable() {
    var x, txt, teksti= "";

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var summa=0;
            var tilaukset = JSON.parse(xhttp.responseText);

            txt = "<table border='1'>";
            txt += "<tr><td> Tilaaja </td><td> Aika </td><td> Tuotteet </td><td> Tilausteksti </td><td> Hinta </td><td> Kuljetus </td><td> Tila </td></tr>"; //Lisätään sarakkeiden otsikot
            var tilaus;

            for (x in tilaukset) { //For looppi käy kaikki tilaukset läpi
                tilaus = tilaukset[x];
                teksti ="";
                var tuotteet = JSON.parse(tilaus['tuotteet']);

                var hinta = 0;
                for (i in tuotteet) {//For looppi käy kaikki tuotteet läpi tilauksessa
                    hinta += tuotteet[i].price *tuotteet[i].amount ;
                    teksti += tuotteet[i].amount+"x "
                    if(tuotteet[i]['foodtype'] === "pizza"){
                        teksti += "<b>pizza:</b> ";
                    }
                    for (x in tuotteet[i]['information']){ //For looppi käy tuotteen informaatiot läpi (Pizzassa useampi täyte, muissa taulukonkoko 1)

                        if(tuotteet[i]['foodtype'] != "pizza") { //Pizzassa useampitäyte, joten käsiteltävä erikseen.
                            teksti += "<b>"+tuotteet[i]['foodtype'] + "</b>: "+ tuotteet[i]['information'][x];
                        } else {
                            teksti += tuotteet[i]['information'][x]+" ";
                        }
                    }
                    teksti += "<br>";
                }
                //Lisätään tilaus taulukkoon riville.
                txt += "<tr><td>" + tilaus.tilaaja+ "</td><td>"+tilaus.aika+"</td><td>"+teksti+"</td><td>"+tilaus.teksti+
                    "</td><td>"+hinta+"€"+"</td><td>"+tilaus.kuljetus+"</td><td>"+tilaus.tila+"</td></tr>";

                //Lisätään tämänhetkisen tilauksen hinta kokonaissummaan
                summa +=hinta;
            }
            txt += "</table>";
            //Lisätään taulukko html elementtiin.
            document.getElementById("ordertable").innerHTML = txt;

        }
    };
    xhttp.open("POST", "php/kayttajasivu.php?tunnus="+tunnus, true);
    xhttp.send();
}