var tilaukset;
//Tarkistetaan, onko kirjautunut käyttäjä admin.
if(sessionStorage.getItem("tunnus")!="admin"){
    alert("Vain admin pääsee tälle sivulle")
    window.location.href = "frontpage.html";
}

//Katsotaan onko käyttäjä kirjautunut ja muutetaan kirjautumisnapin tekstiä.
if(sessionStorage.getItem("tunnus")){
    var tunnus = sessionStorage.getItem("tunnus");
    document.getElementById("index").innerHTML = "Kirjaudu ulos ("+tunnus+")";
} else {
    document.getElementById("index").innerHTML ="Kirjaudu sisään";
}

//Taulukoiden päivitys funktio
function updateTables () {
    updateUserTable();
    updateOrderTable();
}

//Käyttäjien poisto funktio
function deleteUsers() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(xhttp.responseText >= 1){ //Php palauttaa muutettujen rivien lukumäärän
            updateUserTable();
            document.getElementById("usertable").innerHTML = "";
            alert("Käyttäjät poistettu");
            } else {
                alert("Ei ollut käyttäjiä mitä poistaa");
            }
        }
    };
    xhttp.open("GET", "php/deleteUsers.php", true);
    xhttp.send();
}

//Käyttäjätaulukon päivitysfunktio
function updateUserTable() {
    var myObj, x, txt = "";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            myObj = JSON.parse(xhttp.responseText);

            txt += "<table border='1'>";
            txt += "<tr><td> Tunnus </td><td> Salasana </td></tr>"; //Muodostetaan taulukon sarakkeiden otsikot
            for (x in myObj) {
                txt += "<tr><td>" + myObj[x].tunnus + "</td><td>"+myObj[x].salasana+"</td></tr>"; //Lisätään taulukkoon rivit
            }
            txt += "</table>"
            document.getElementById("usertable").innerHTML = txt; //Lopuksi lisätään taulukko html elementtiin.

        }
    };
    xhttp.open("POST", "php/admin.php", true);
    xhttp.send();
}

//Tilauslistan päivitysfunktio
function updateOrderTable() {
    var txt,teksti = "";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var summa=0;
            tilaukset = JSON.parse(xhttp.responseText);

            txt = "<table border='1'>";
            txt += "<tr><td> Tilaaja </td><td> Aika </td><td> Tuotteet </td><td> Tilausteksti </td><td> Hinta </td><td> Kuljetus </td><td> Tila </td></tr>"; //Muodostetaan taulukon sarakkeiden otsikot

            var tilaus;
            for (x in tilaukset) {
                tilaus = tilaukset[x];
                teksti ="";
                var tuotteet = JSON.parse(tilaus['tuotteet']);
                var hinta = 0;

                for (i in tuotteet) {
                    hinta += tuotteet[i].price * tuotteet[i].amount;
                    teksti += tuotteet[i].amount+"x "
                    if(tuotteet[i]['foodtype'] === "pizza"){ //Pizzalla on monta täytettä, joten se pitää käsitellä erikseen
                        teksti += "<b>pizza:</b> ";
                    }

                    for (z in tuotteet[i]['information']){

                        if(tuotteet[i]['foodtype'] != "pizza") {//Pizzalla on monta täytettä, joten se pitää käsitellä erikseen
                            teksti += "<b>"+tuotteet[i]['foodtype'] + " </b>: "+ tuotteet[i]['information'][z];
                        } else {
                            teksti += tuotteet[i]['information'][z]+" ";
                        }
                    }
                    teksti += "<br>";
                }

                var nappi = document.createElement("button"); //Luodaan nappi tilauksen hyväksymistä varten
                nappi.id = "button"+x.toString();
                nappi.innerHTML = "Hyväksy tilaus";

                if (tilaus.tila == 'Tilaus hyväksytty') { //Poistetaan nappi, jos tilaus on hyväksytty
                    nappi.style.display = "none";
                }

                txt += "<tr><td>" + tilaus.tilaaja+ "</td><td>"+tilaus.aika+"</td><td>"+teksti+"</td><td>"+tilaus.teksti+"</td><td>"+hinta+"€"+
                    "</td><td>"+tilaus.kuljetus+"</td><td>"+tilaus.tila+"</td><td>"+nappi.outerHTML+"</td></tr>"; //Lisätään taulukkoon rivit
                summa +=hinta;
            }
            txt += "</table>";
            document.getElementById("ordertable").innerHTML = txt; //Lopuksi lisätään taulukko html elementtiin.
            document.getElementById("kassa").innerHTML = "Pizzapaikan kassassa on "+summa+"€"; //Päivitetään pizzapaikan kassan summaa

            functionate(); //Lopuksi annetaan lisätyille nappululoille eventlistenerit

        }
    };
    xhttp.open("POST", "php/ordertable.php", true);
    xhttp.send();
}
//Nappuloiden funktio, millä hyväksytään tilaus.
function paivitaTila(tunnus, aika) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (xhttp.responseText > 0) { //Php palauttaa muuttuneiden rivinen määrän.
                updateOrderTable();
            } else {
                alert("Tilauksen tilanvaihto epäonnistui");
            }
        }
    };
    xhttp.open("POST", "php/changeStatus.php?tunnus="+tunnus+"&aika="+aika, true);
    xhttp.send();
}

//Funktio, mikä antaa kaikille tilaustaulukon nappuloille eventlistenerin
function functionate() {
    for (y in tilaukset) {
        document.getElementById("button"+y).addEventListener("click", function () {
            var id = this.id.split("button")[1];
            paivitaTila(tilaukset[id].tilaaja, tilaukset[id].aika);
        })
    }
}