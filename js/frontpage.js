var piilota = document.getElementById("piilota");
var hampurilainen = document.getElementById("hampurilainen");
var pizza = document.getElementById("pizza");
var salaatti = document.getElementById("salaatti");
var kebab = document.getElementById("kebab");
var ruokalisää = document.getElementById("ruokalisää");
var ruokavähennä = document.getElementById("ruokavähennä");
var ruokatilausmäärä = document.getElementById("ruokatilausmäärä");
var juomalista = document.getElementById("juomalista");
var ostoskorinappi = document.getElementById("ostoskorinappi");
var hampurilaistyyppi = document.getElementById("hampurilaistyyppi");
var salaattityyppi = document.getElementById("salaattityyppi");
var kebabtyyppi = document.getElementById("kebabtyyppi");
var normi = document.getElementById("normi");
var perhe = document.getElementById("perhe");
var täyte1 = document.getElementById("täyte1");
var täyte2 = document.getElementById("täyte2");
var täyte3 = document.getElementById("täyte3");
var täyte4 = document.getElementById("täyte4");
var oregano = document.getElementById("oregano");
var valkosipuli = document.getElementById("valkosipuli");
var tuplajuusto = document.getElementById("tuplajuusto");
var ostoskoridiv = document.getElementById("ostoskori");
var juomat = document.getElementById("juomat");
var lukumäärädiv = document.getElementById("lukumäärädiv");
var loppusumma = document.getElementById("loppusumma");
var tuotevalikoima = ["hampurilainen", "kebab", "pizza", "salaatti", "juoma"];
var valittutuote = 0;
var ruokatilauslkm = 1;
var ostoskori = [];
var distance= 16;
var kuljetusPaikka="";
ostoskorinappi.style.display = "none";
piilota.style.display = "none";
lukumäärädiv.style.display = "none";

//Tarkistetaan onko käyttäjä kirjautunut.
if(sessionStorage.getItem("tunnus")){
    var tunnus = sessionStorage.getItem("tunnus");
    document.getElementById("tunnusteksti").innerHTML ="Kirjautunut tunnuksella: <b>"+tunnus+"</b>";
    document.getElementById("index").innerHTML = "Kirjaudu ulos ("+tunnus+")";
} else {
    document.getElementById("tunnusteksti").innerHTML = "Et ole kirjautunut sisään";
    document.getElementById("index").innerHTML ="Kirjaudu sisään";
}

// Määritellään, mitkä elementit näkyvät kebabin kuvaa klikkaamalla
document.getElementById("kebukuva").onclick = function() {
    ostoskorinappi.style.display = "block";
    piilota.style.display = "block";
    hampurilainen.style.display = "none";
    pizza.style.display = "none";
    salaatti.style.display = "none";
    juomat.style.display = "none";
    kebab.style.display = "block";
    lukumäärädiv.style.display = "block";
    valittutuote = 2;
};

// Määritellään, mitkä elementit näkyvät hampurilaisen kuvaa klikkaamalla
document.getElementById("burgerikuva").onclick = function() {
    piilota.style.display = "block";
    kebab.style.display = "none";
    pizza.style.display = "none";
    juomat.style.display = "none";
    salaatti.style.display = "none";
    hampurilainen.style.display = "block";
    lukumäärädiv.style.display = "block";
    ostoskorinappi.style.display = "block";
    valittutuote = 1;
};

// Määritellään, mitkä elementit näkyvät salaatin kuvaa klikkaamalla
document.getElementById("salaattikuva").onclick = function() {
    ostoskorinappi.style.display = "block";
    piilota.style.display = "block";
    hampurilainen.style.display = "none";
    kebab.style.display = "none";
    pizza.style.display = "none";
    juomat.style.display = "none";
    salaatti.style.display = "block";
    lukumäärädiv.style.display = "block";
    valittutuote = 4;
};

// Määritellään, mitkä elementit näkyvät pizzan kuvaa klikkaamalla
document.getElementById("pizzakuva").onclick = function() {
    ostoskorinappi.style.display = "block";
    piilota.style.display = "block";
    hampurilainen.style.display = "none";
    salaatti.style.display = "none";
    kebab.style.display = "none";
    juomat.style.display = "none";
    pizza.style.display = "block";
    lukumäärädiv.style.display = "block";
    valittutuote = 3;
};

// Määritellään, mitkä elementit näkyvät  juoman kuvaa klikkaamalla
document.getElementById("juomakuva").onclick = function() {
    ostoskorinappi.style.display = "block";
    piilota.style.display = "block";
    hampurilainen.style.display = "none";
    kebab.style.display = "none";
    pizza.style.display = "none";
    salaatti.style.display = "none";
    juomat.style.display = "block";
    lukumäärädiv.style.display = "block";
    valittutuote = 5;
};

function updateNakyma() {
    piilota.style.display = "none";
    lukumäärädiv.style.display = "none";
}

// Vähentää valitun tuotteen määrää yhdellä
ruokavähennä.onclick = function() {
    if (ruokatilauslkm > 1) {
        ruokatilauslkm--;
    }
    ruokatilausmäärä.innerHTML = ruokatilauslkm;
};

// Kasvattaa valitun tuotteen määrää yhdellä
ruokalisää.onclick = function() {
    if (ruokatilauslkm < 20) {
        ruokatilauslkm++;
    } else {
        alert("Maksimimäärä 20 kpl");
    }
    ruokatilausmäärä.innerHTML = ruokatilauslkm;
};

// Lisää ostoskoriin tuotteen
ostoskorinappi.onclick = function() {
    if (valittutuote === 0) {
        alert("Tuotetta ei valittu");
        return;
    } else if (valittutuote === 1) {
        if (hampurilaistyyppi.selectedIndex != 0) {
            var valittulaji = [hampurilaistyyppi[hampurilaistyyppi.selectedIndex].value];
        } else {
            alert("Valitse hampurilaisen täyte");
            return;
        }

    } else if (valittutuote === 2) {
        if (kebabtyyppi.selectedIndex != 0) {
            var valittulaji = [kebabtyyppi[kebabtyyppi.selectedIndex].value];
        } else {
            alert("Valitse kebabin täyte");
            return;
        }
    } else if (valittutuote === 3) {
        var pizzatiedot = [];
        if (normi.checked == true) {
            var pizzakoko = "Normaalipizza";
        } else if (perhe.checked == true) {
            var pizzakoko = "Perhepizza";
        }
        pizzatiedot.push(pizzakoko);
        if (!(täyte1.value === "eitäytettä")) {
            pizzatiedot.push(täyte1[täyte1.selectedIndex].value);
        }
        if (!(täyte2.value === "eitäytettä")) {
            pizzatiedot.push(täyte2[täyte2.selectedIndex].value);
        }
        if (!(täyte3.value === "eitäytettä")) {
            pizzatiedot.push(täyte3[täyte3.selectedIndex].value);
        }
        if (!(täyte4.value === "eitäytettä")) {
            pizzatiedot.push(täyte4[täyte4.selectedIndex].value);
        }
        if (oregano.checked == true) {
            pizzatiedot.push("oregano");
        }
        if (valkosipuli.checked == true) {
            pizzatiedot.push("valkosipuli");
        }
        if (tuplajuusto.checked == true) {
            pizzatiedot.push("tuplajuusto");
        }

        var valittulaji = pizzatiedot;
    } else if (valittutuote === 4) {
        if (salaattityyppi.selectedIndex != 0) {
            var valittulaji = [salaattityyppi[salaattityyppi.selectedIndex].value];
        } else {
            alert("Valitse salaatin täyte");
            return;
        }

    } else if (valittutuote === 5) {
        if (juomalista.selectedIndex != 0) {
            var valittulaji = [juomalista[juomalista.selectedIndex].value];
        } else {
            alert("Valitse juoma");
            return;
        }

    }

    var tuotetyyppi = tuotevalikoima[valittutuote-1];
    var hinta = 0;

    // Lasketaan valitulle tuotteelle hinta
    if (tuotetyyppi === "hampurilainen") {
         hinta = 6;
    } else if (tuotetyyppi === "kebab") {
        hinta = 8;
    } else if (tuotetyyppi === "pizza") {
        hinta = 5;
        if (pizzakoko === "Perhepizza") {
            hinta += 4;
        }
        hinta += pizzatiedot.length-1;
    } else if (tuotetyyppi === "salaatti") {
        hinta = 5;
    } else if (tuotetyyppi === "juoma") {
        hinta = 3;
    }

    var ostokset = {foodtype: tuotetyyppi, information: valittulaji, amount: ruokatilauslkm,price: hinta };

    // Nollataan valittu tuote
    valittutuote = 0;
    // Asetetaan valittujen tuotteiden lukumäärä oletukseen yksi
    ruokatilauslkm = 1;
    ruokatilausmäärä.innerHTML = ruokatilauslkm;

    if (ostoskori.length < 10) {
        ostoskori.push(ostokset);
    } else {
        alert("Ostoskori täynnä!");
    }
    updateBasket();
    document.getElementById("ostosform").reset();
    updateNakyma();

};
//Päivittää ostoskorin.
function updateBasket() {
    //Ensin tyhjennetään ostoskorin html elementti
    var myNode = document.getElementById("ostoskori");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }

    var textline = "";
    for (tilaus in ostoskori) {
        //Tiedot string luonti (Pizzassa on usampi täyte)
        var tiedot = "";
            for (var i = 0; i<ostoskori[tilaus].information.length;i++) {
                tiedot += ostoskori[tilaus].information[i] + ", ";
            }

        textline = document.createTextNode(ostoskori[tilaus].foodtype + ", " + tiedot + ostoskori[tilaus].amount + "kpl, " + ostoskori[tilaus].price * ostoskori[tilaus].amount + "€");

        var newtr = document.createElement("tr");
        newtr.id = tilaus; //Annetaan riville id. Hyödynnetään kun poistetaan ostoskorista tuote.

        var texttd = document.createElement("td");

        var deleteButton = document.createElement("button");
        deleteButton.innerHTML="Poista tuote";
        deleteButton.id = "button"+tilaus;

        texttd.appendChild(textline);
        newtr.appendChild(texttd);
        newtr.appendChild(deleteButton);
        ostoskoridiv.appendChild(newtr);

        document.getElementById("button"+tilaus).addEventListener("click", function () { rem(newtr.id); });
    }
    //Päivitetään tilauksen loppusummaa
    var summa = 0;
    for (tuote in ostoskori) {
        summa += ostoskori[tuote].price *ostoskori[tuote].amount;
    }
    loppusumma.innerHTML = summa + "€";

}

//Ostoskorin tilauksen poistofunktio
function rem(id) {
    if (document.getElementById(id)) {
        ostoskori.splice(id, 1);
        updateBasket();
    }
}

//Tilauksenteko funktio
function sendOrder() {
    //Tilaaja on oletuksena tuntematon
    var tunnus = "tuntematon";

    if(ostoskori.length === 0){
        alert("Ostoskori on tyhjä");
        return;
    }

    if(sessionStorage.getItem("tunnus")){
        tunnus = sessionStorage.getItem("tunnus");
    }

    //Muodostetaan tilauksen aika
    var date = new Date();
    var tunti = date.getHours();
        if(date.getHours()<10) {
            tunti = "0"+tunti;
        }
    var minuutti = date.getMinutes();
        if(date.getMinutes()<10){
            minuutti = "0"+minuutti;
        }
    var aika = date.getDate() +"."+(date.getMonth()+1)+"."+date.getFullYear()+" klo: "+tunti+"."+minuutti;


    var jsonkori = JSON.stringify(ostoskori);
    var teksti = document.getElementById("lisatietoja");

    //Tarkastetaan kotiinkuljetukseen liittyvät tarkistukset
    if (document.getElementById("kotiink").checked){
        if (kuljetusPaikka === ""){
            alert("Kuljetusosoitetta ei asetettu");
            return;
        }
        if (distance >15){
            alert("Kuljetusmatka on liian suuri");
            return;
        }
    } else {
        kuljetusPaikka = "Ei kuljetusta";
    }
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (xhttp.responseText == 1) { //Php palauttaa muuttuneiden rivien lukumäärän
                alert("Tilaus vastaanotettu");
                window.location.href="frontpage.html";
            } else {

                alert("Tilaus epäonnistui");
            }
        }
    };
    xhttp.open("POST", "php/sendorder.php?ostoskori="+jsonkori+"&teksti="+teksti.value+"&tunnus="+tunnus+"&aika="+aika+"&kuljetus="+kuljetusPaikka, true);
    xhttp.send();
}

//Kartan piiloittaminen ja esiintuonti
function hideMap() {
    document.getElementById("map").style.display = "none"
    document.getElementById("etaisyys").innerHTML="";
    if(document.getElementById("kotiink").checked){
        document.getElementById("map").style.display = "block"
    }
}

//Loput koodista saatu suoraan netistä osoitteista https://leafletjs.com/ ja https://cloud.maptiler.com/geocoding/
var mymap = L.map('mapid').setView([60.163082, 24.931868], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

var autocomplete = new kt.OsmNamesAutocomplete(
    'search', 'https://geocoder.tilehosting.com/', '2HgwdXRGNqTA5z5Y0p2z');
var myIcon = L.icon({
    iconUrl: 'img/pizza.png',
    iconSize: [38, 55],
    iconAnchor: [22, 55],
    popupAnchor: [-3, -76],
});
var pizzapaikka = [60.163082, 24.931868]; //Ruokapaikan koordinaatit
var marker = L.marker([0,0]);
var mark = L.marker(pizzapaikka, {icon: myIcon}).addTo(mymap);
mark.addTo(mymap);
marker.addTo(mymap);
autocomplete.registerCallback(function(item) {
    var coords = [item.lat, item.lon];
    kuljetusPaikka = item.name; //Kotiinkuljetuspaikan nimi
    mymap.flyTo(coords);
    marker.setLatLng(coords);
    distance = mymap.distance(mark.getLatLng(),marker.getLatLng());
    distance = distance/1000;
    distance = distance.toFixed(2);
    //Päivitetään etäisyydenkertovaa tekstiä
    if(distance <= 15.0){
        document.getElementById("etaisyys").innerHTML = "Etäisyys on "+distance+" kilometriä.<br>Kotiinkuljetus on mahdollista";
    } else {
        document.getElementById("etaisyys").innerHTML = "Etäisyys on "+distance+" kilometriä.<br>Kotiinkuljetus ei ole mahdollista";
    }

});
//Piiloitetaan kartta aluksi.
document.getElementById("map").style.display = "none";